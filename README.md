File Parser<br/>
=============

Requirements
=============
* python3

Configurations
=============
Step 1,
Create Virtual Environment
```
make venv
```

Step 2,
Run Virtual Environment
```
source .venv/bin/activate
```

Step 3,
Install dependencies
```
make init
```

Run the application
=============
# Run the main.py program
```
make run
```

# Run using command line
You can use ./src/cmd path/to/data.txt path/to/specs.csv<br>
Example
```
./src/cmd data/sample_2022-05-25.txt specs/sample.csv
```

Test the application
=============
# Unit test
```
make test
```

# Coverage report
```
coverage run -m pytest && coverage report -m
```

Clean up artifacts
=============
```
make clean
```

Exit the virtual environment
=============
```
deactivate
```
