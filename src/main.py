import fileparser

if __name__ == "__main__":
    # Parse specfic data and spec file
    output = fileparser.parse_flatfile(
        'data/sample_2022-05-25.txt', 'specs/sample.csv')
    print(output)

    # Parse all the files inside data and specs folder
    output = fileparser.parse_all(
        'data/', 'specs/')
    for o in output:
        print(o)
