import os
import pandas as pd
import logging

# Set up logger
logging.basicConfig(filename='message.log',
                    level=logging.INFO,
                    format='%(asctime)s:%(levelname)s:%(message)s')


class Spec:
    """
    Spec is a class for the specification file
    """

    def __init__(self, column: str, width: int, datatype: str):
        self.column = column
        self.width = width
        self.datatype = datatype

    def dict(self):
        return {'column': self.column, 'width': self.width, 'datatype': self.datatype}


def parse_line(line: str, specs: str):
    """
    parse_line is a function to read the data row based on the specification
    """

    row = {}
    read_idx = 0
    for s in specs:
        # Read the value according to the width
        val = line[read_idx:read_idx + s['width']].strip()

        # Parse the value according to the datatype
        parsed_val = parse_type(s['datatype'], val)
        read_idx = read_idx + s['width']

        # Check for error
        if parsed_val == None:
            logging.error(
                'Failed to parse value: {val}, line: {line}'.format(val=val, line=line))
            return

        # Store in object if no error
        row[s['column']] = parsed_val

    return row


def parse_type(cur_type: str, val: str):
    """
    parse_type is a function to return the value according to the type
    type: TEXT, BOOLEAN, INTEGER
    """

    if cur_type == 'TEXT':
        return parse_str(val)

    if cur_type == 'BOOLEAN':
        return parse_bool(val)

    if cur_type == 'INTEGER':
        return parse_int(val)

    return None


def parse_str(val: str):
    """
    parse_str is a function to return string
    """
    return str(val)


def parse_bool(val: str):
    """
    parse_bool is a function to return boolean
    """
    if (val == '0'):
        return False
    elif (val == '1'):
        return True
    return None


def parse_int(val: str):
    """
    parse_int is a function to return integer
    """
    try:
        return int(val)
    except:
        return None


def parse_flatfile(datafilename: str, formatfilename: str):
    """
    parse_flatfile is a function to parse data file based on the specification file
    """
    logging.info('Parsing datafile: {datafilename}, formatfilename: {formatfilename}'.format(
        datafilename=datafilename, formatfilename=formatfilename))

    # Read spec and datafile
    try:
        spec_df = pd.read_csv(formatfilename)
        datafile = open(datafilename, 'r')

        # Convert spec dataframe into array of dictionary
        specs = spec_df.apply(lambda x: Spec(x['column name'],
                                             x['width'], x['datatype']).dict(), axis=1)

        # Process each line in the datafile to get the output
        output = []
        for line in datafile:
            cur_line = parse_line(line.strip(), specs)
            if cur_line:
                output.append(cur_line)

        return output
    except Exception as e:
        logging.error(
            'Failed to parse file, error: {err}'.format(err=e))
        return None


def parse_all(datafolder: str, formatfolder: str):
    """
    parse_all is a function to parse all files inside data folder and specification folder
    """
    all_output = []
    try:
        for formatfile in os.listdir(formatfolder):
            formatfile_path = os.path.join(formatfolder, formatfile)
            # checking if it is a file
            if os.path.isfile(formatfile_path):
                # find datafiles based on spec filename
                datafiles = findfile(
                    datafolder, os.path.splitext(formatfile)[0])

                # parse datafile based on each spec
                for d in datafiles:
                    output = parse_flatfile(d, formatfile_path)
                    if output != None:
                        all_output.append(output)
        return all_output
    except Exception as e:
        logging.error(
            'Failed to parse all file, error: {err}'.format(err=e))
        return []


def findfile(path: str, keyword: str):
    """
    findfile is a function to find all file path that contains the keyword
    """

    files = []
    for curfile in os.listdir(path):
        curfile_path = os.path.join(path, curfile)
        # checking if it is a file
        if os.path.isfile(curfile_path) and keyword == get_datafile_prefix(curfile):
            files.append(curfile_path)
    return files


def get_datafile_prefix(filename):
    f = filename.split("_")
    if len(f) > 1:
        return f[0]
    return None
