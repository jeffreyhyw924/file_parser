import fileparser


class TestSpecClass:
    def test_spec_class_valid_dict(self):
        s = fileparser.Spec(column='name', datatype='TEXT', width=10)
        assert s.dict() == {'column': 'name', 'width': 10, 'datatype': 'TEXT'}


class TestParseLine:
    specs = [{'column': 'name', 'width': 10, 'datatype': 'TEXT'}, {'column': 'valid',
                                                                   'width': 1, 'datatype': 'BOOLEAN'}, {'column': 'count', 'width': 3, 'datatype': 'INTEGER'}]

    def test_parse_line_valid(self):
        assert fileparser.parse_line("Stroke    1103", self.specs) == {
            'name': 'Stroke', 'valid': True, 'count': 103}

    def test_parse_line_invalid_1(self):
        assert fileparser.parse_line("Stroke    a103", self.specs) == None

    def test_parse_line_invalid_2(self):
        assert fileparser.parse_line("Stroke    0  b", self.specs) == None


class TestParseType:
    def test_parse_type_int_valid(self):
        assert fileparser.parse_type("INTEGER", "10") == 10

    def test_parse_type_str_valid(self):
        assert fileparser.parse_type("TEXT", "10") == "10"

    def test_parse_type_bool_valid(self):
        assert fileparser.parse_type("BOOLEAN", "0") == False

    def test_parse_type_invalid(self):
        assert fileparser.parse_type("UKNOWN", "0") == None


class TestParseBool:
    def test_parse_bool_true(self):
        assert fileparser.parse_bool('1') == True

    def test_parse_bool_false(self):
        assert fileparser.parse_bool('0') == False

    def test_parse_bool_invalid_1(self):
        assert fileparser.parse_bool(0) == None

    def test_parse_bool_invalid_2(self):
        assert fileparser.parse_bool('a') == None


class TestParseInt:
    def test_parse_int_valid_1(self):
        assert fileparser.parse_int('10') == 10

    def test_parse_int_valid_2(self):
        assert fileparser.parse_int('-10') == -10

    def test_parse_int_invalid(self):
        assert fileparser.parse_int('a') == None


class TestParseStr:
    def test_parse_int_valid_1(self):
        assert fileparser.parse_str('hello') == 'hello'

    def test_parse_int_valid_2(self):
        assert fileparser.parse_str('-10') == '-10'


class TestParseFlatfile:
    def test_parse_flatfile_valid(self):
        output = fileparser.parse_flatfile(
            'test/data/sample_2022-05-25.txt', 'test/specs/sample.csv')
        assert output == [{'name': 'Diabetes', 'valid': True, 'count': 1}, {
            'name': 'Asthma', 'valid': False, 'count': -12}, {'name': 'Stroke', 'valid': True, 'count': 103}]

    def test_parse_flatfile_invalid(self):
        output = fileparser.parse_flatfile(
            'test/data/sample_2022-05-25_invalid.txt', 'test/specs/sample.csv')
        assert output == [{'name': 'Diabetes', 'valid': True, 'count': 1}]

    def test_parse_flatfile_invalid_path(self):
        output = fileparser.parse_flatfile(
            'test/data/not_exist.txt', 'test/specs/sample.csv')
        assert output == None


class TestParseAll:
    def test_parse_all_valid(self):
        output = fileparser.parse_all(
            'test/data/', 'test/specs/')
        assert output == [
            [{'name': 'Diabetes', 'valid': True, 'count': 1}],
            [{'name': 'Diabetes', 'valid': True, 'count': 1}, {
                'name': 'Asthma', 'valid': False, 'count': -12}, {'name': 'Stroke', 'valid': True, 'count': 103}],
            [{'name': 'Diabetes', 'another name': 'DBTS', 'valid': False, 'count': 103}, {
                'name': 'Asthma', 'another name': 'ASTH', 'valid': True, 'count': -12}, {'name': 'Stroke', 'another name': 'STRK', 'valid': False, 'count': 1}]]

    def test_parse_all_invalid(self):
        output = fileparser.parse_all(
            'test/unknowndata/', 'test/uknownspecs/')
        assert output == []


class TestFindFile:
    def test_findfile_valid_1(self):
        output = fileparser.findfile(
            'test/data/', 'sample')
        assert len(output) == 2

    def test_findfile_valid_2(self):
        output = fileparser.findfile(
            'test/data/', 'specialsample')
        assert len(output) == 1


class TestFindFile:
    def test_get_datafile_prefix_valid_1(self):
        output = fileparser.get_datafile_prefix(
            'sample_2022-05-25_invalid.txt')
        assert output == 'sample'

    def test_get_datafile_prefix_valid_2(self):
        output = fileparser.get_datafile_prefix(
            'sample2_2022-05-25_invalid.txt')
        assert output == 'sample2'

    def test_get_datafile_prefix_invalid(self):
        output = fileparser.get_datafile_prefix('')
        assert output == None
