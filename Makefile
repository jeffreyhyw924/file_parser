.PHONY: activate
activate:
	. .venv/bin/activate                                                

.PHONY: venv
venv:
	python3 -m venv .venv

.PHONY: init
init: activate install
	
.PHONY: install
install:
	pip install -r requirements.txt
	
.PHONY: run
run:
	python src/main.py
	
.PHONY: test
test:
	py.test -v

.PHONY: clean
clean:
	rm -rf src/.pytest_cache && rm -rf src/__pycache__/ && rm -rf .venv && rm -rf .coverage
